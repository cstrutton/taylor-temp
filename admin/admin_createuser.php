<?php
	require_once("phpscripts/init.php");
	//confirm_logged_in();
	
	if(isset($_POST['submit'])) {
		//echo "works";
		$fname = trim($_POST['fname']);
		$lname = trim($_POST['lname']);
		$username = trim($_POST['username']);
		$password = passGen(12);
		$level = $_POST['lvllist'];
		$email = trim($_POST['email']);
		
		if(empty($level)) {
			$message = "Please select a user level.";
		}else{
			//echo "all good...";
	$result = createUser($fname, $lname, $username, $password, $level, $email);
			$message = $result;
		}
		
	}

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Create New User</title>
<link rel="stylesheet" href="../../css/foundation.css" type="text/css" />
<link rel="stylesheet" href="css/admin_index.css" type="text/css" />
<script type="text/javascript" src="../../js/vendor/foundation.js"></script>
<script type="text/javascript" src="../../js/vendor/jquery.js"></script>
<script type="text/javascript" src="../../js/vendor/what-input.js"></script>
</head>
<body>
	
		<header class="expanded row">
		<h1>Welcome <?php echo $_SESSION['user_fname'];  ?> to your admin panel</h1>
	</header>
	
	<div class="expanded row">
		<div class="large-2 columns" id="menu">
		    <a href="admin_createuser.php">Create New User</a>
		    <a href="admin_edituser.php">Edit Account</a>
		    <a href="#">Change Password</a>
		    <a href="phpscripts/caller.php?caller_id=logout">Sign Out</a>
	    </div>
	
	<div class="large-3 large-offset-3 columns end" id="create">
	<h3>Create New User</h3>
    <?php if(!empty($message)){echo $message;} ?>
	<form action="admin_createuser.php" method="post">
		<label>First Name:</label>
        <input type="text" name="fname" value="<?php if(!empty($fname)){echo $fname;} ?>">
        <label>Last Name:</label>
        <input type="text" name="lname" value="<?php if(!empty($lname)){echo $lname;} ?>">
        <label>Username:</label>
        <input type="text" name="username" value="<?php if(!empty($username)){echo $username;} ?>">
        <label>Email Address:</label>
        <input type="email" name="email" value="<?php if(!empty($email)){echo $email;} ?>">
	
  		
        <select name="lvllist">
        	<option value="">Please select a user level...</option>
        	<option value="2">Web Admin</option>
            <option value="1">Web Master</option>
        </select>
        <input type="submit" name="submit" value="Create User" class="button">
        </form>
    </div>    
        </div>
        
    <script type="text/javascript" src="../../js/app.js"></script>

</body>
</html>