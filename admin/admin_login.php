<?php
	//ini_set('display_errors', 1);
	//error_reporting(E_ALL);
	
	$ip = $_SERVER["REMOTE_ADDR"];
	//echo $ip;
	
	require_once("phpscripts/init.php");
	
	if(isset($_POST['submit'])) {
		//echo "Congrats, you're a good clicker!";
		$username = trim($_POST['username']);
		$password = trim($_POST['password']);
		
		if($username != "" && $password != "") {
			$result = logIn($username, $password, $ip);
			$message = $result;
		}else{
			$message = "Please fill in the required fields.";
		}
	}
	
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome Company Name</title>
<link rel="stylesheet" href="../../css/foundation.css" type="text/css" />
<link rel="stylesheet" href="css/admin_login.css" type="text/css" />
<script type="text/javascript" src="../../js/vendor/foundation.js"></script>
<script type="text/javascript" src="../../js/vendor/jquery.js"></script>
<script type="text/javascript" src="../../js/vendor/what-input.js"></script>
</head>
<body>
	<div class="large-3 large-centered columns" id="login">
	<h1>Admin Login</h1>
    <?php if(!empty($message)) {echo $message;} ?>
	<form action="admin_login.php" method="post">
    	<label>Username:</label>
    	<input type="text" name="username" value="">
        <label>Password:</label>
    	<input type="password" name="password" value="">
        <input type="submit" name="submit" value="Login" class="button">
    </form>
    </div>
    <script type="text/javascript" src="../../js/app.js"></script>
</body>
</html>