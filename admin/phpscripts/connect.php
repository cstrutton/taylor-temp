<?php
	//Set up connection credentials
	// $user = "taylor132465";
	// $pass = "";
	// $url = "localhost";
	// $db = "db_bluray";
	
	//$link = mysqli_connect($url, $user, $pass, $db); //PC
	//$link = mysqli_connect($url, $user, $pass, $db, "8889"); //Mac

	$url = parse_url(getenv("CLEARDB_DATABASE_URL"));

	$server = $url["host"];
	$username = $url["user"];
	$password = $url["pass"];
	$db = substr($url["path"], 1);

	$link = new mysqli($server, $username, $password, $db);
	
	
	
	/* check connection */ 	
	if(mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
?>