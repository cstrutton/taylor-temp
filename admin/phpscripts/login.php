<?php
	//echo "login.php";
	function logIn($username, $password, $ip) {
		require_once("connect.php");
		
		//is user in db
		$username = mysqli_real_escape_string($link,$username);
		$password = mysqli_real_escape_string($link,$password);
		$loginQueryString = "SELECT * FROM tbl_user WHERE user_name='{$username}'";
		//echo $loginString;
		$user_set = mysqli_query($link, $loginQueryString);
		
		$failed = false;
		
		if(mysqli_num_rows($user_set) == 0) {
			$failed = true;
		}else{
			$found_user = mysqli_fetch_array($user_set, MYSQLI_ASSOC);
		}
		//does password match
		if($failed == false) {
			//echo $found_user['user_fname'];
			if(password_verify($password, $found_user['user_pass'])){
				write_log('password verified');
			}else{
				$failed = true;
			}
		}
			
		if ($failed == false){
			$id = $found_user['user_id'];
			$lastLog = $found_user['user_currentLog'];
			$_SESSION['users_creds'] = $id;
			$_SESSION['users_name'] = $found_user['user_name'];
			//if(mysqli_query($link, $loginQueryString)) {
			$updateString = "UPDATE tbl_user SET " .
							"user_ip='{$ip}', " .
							"user_lastLog = '{$lastLog}', ".
							"user_currentLog = NOW(), ".
							"WHERE user_id={$id}";
			$updateQuery = mysqli_query($link, $updateString);
			//}
			redirect_to("admin_index.php");	
		}else{
			$message = "Username/Password was incorrect.";	
			return $message;
		}
		mysqli_close($link);
	}
?>