<?php
	// password hashing in php: http://php.net/manual/en/book.password.php
	function editUser($id, $fname, $lname, $username, $password, $email) {
		include("connect.php");
		$updatestring = "UPDATE tbl_user SET ".
						"user_fname='{$fname}', " . 
						"user_lname='{$lname}', " .
						"user_name='{$username}', " .
						"user_pass='{$password}', " .
						"user_email='{$email}' " .
						"WHERE user_id={$id}";

		$updatequery = mysqli_query($link, $updatestring);
		
		if($updatequery) {
			redirect_to("admin_index.php");
		}else{
			$message = "Not going to happen!";
			return $message;
		}
		
		mysqli_close($link);	
	}


	function getUser($id) {
		require_once("connect.php");
		$userstring = "SELECT * FROM tbl_user WHERE user_id={$id}";
		$userquery = mysqli_query($link, $userstring);
		if($userquery){
			$found_user = mysqli_fetch_array($userquery, MYSQLI_ASSOC);
			return $found_user;
		}else{
			$message = "There was a problem finding your account.  Please return all of your access cards to the front desk and get the F out!";
			return $message;
		//return
		}
		mysqli_close($link);	
	}



	function createUser($fname, $lname, $username, $password, $level, $email) {
		require_once("connect.php");
		
		// $queryString = "SELECT * " . 
		// 				"FROM  `tbl_user` " .
		// 				"WHERE user_name =  {'$username'}";
		
		// $result = mysqli_query($link, $queryString);
		// die($result);
		// $userCount = mysqli_num_rows($result);
		
//		if(($result = mysqli_query($link, $queryString))){
			$hashedPassword = password_hash($password, PASSWORD_DEFAULT);
			//string password_hash ( string $password , integer $algo [, array $options ] )
			$ip = '0.0.0.0';
			
			$queryString = "INSERT INTO tbl_user VALUES(NULL,'{$fname}', " .
							"'{$lname}', " .
							"'{$username}', " .
							"'{$hashedPassword}', " .
							"'{$level}', " .
							"'{$ip}', " .
							"'{$email}', " . 
							"NULL, NULL)";  // user_currentLog, user_lastLog
			//die($queryString);
			$result = mysqli_query($link, $queryString);
			if($result) {
				write_log($username.":".$password);
				newUserEmail($fname, $lname, $username, $password, $level, $email);
				redirect_to("admin_index.php");
	
			}else{
				$message = "Your hiring practices have failed you, we can not keep this individual.";
				return $message;
			}
		// }else{
		// 	$message = "I'm sorry, this username is taken.";
		// 	return $message;
		// }
		mysqli_close($link);
	}
?>