<?php
	
	function redirect_to($location) {
		if($location != NULL) {
			header("Location: {$location}");
			exit;
		}
	}

	function addmovie($fimg, $thumb, $title, $year, $storyline, $runtime, $trailer, $price, $cat) {
		include("connect.php");

		//echo "from addMovie()";
		
		$fimg  = mysqli_real_escape_string($link, $fimg);

		if($_FILES['movie_fimg']['type'] == "image/jpg" || $_FILES['movie_fimg']['type'] == "image/jpeg"){
			//echo "this is a jpg";
			
			$targetpath = "../images/{$fimg}";

			if(move_uploaded_file($_FILES['movie_fimg']['tmp_name'], $targetpath)){
				//echo "file moved";
				
				$orig = "../images/{$fimg}";
				$th_copy = "../images/{$thumb}";

				if(!copy($orig, $th_copy)){
					echo "failed to copy";
				}

				$size = getimagesize($orig);
				//echo $size[0]."x".$size[1];

			}
		}else{
			echo "no gifs please";
		}

		mysqli_close($link);
	}
	
	function passGen ($length = 12){
		$lowercase = implode(range('a','z'));
		$uppercase = implode(range('A','Z'));
		$numbers = implode(range(0,9));
		$symbols = ('!@#$%^&*');

		$chars = $lowercase.$uppercase.$numbers.$symbols;
		$password = substr(str_shuffle($chars),0, $length);
		//echo $password;
		return $password;
	}
	
	//echo passGen();
	
	function newUserEmail_SMTP($fname, $lname, $username, $password, $level, $email){
		//echo $fname. $lname. $username. $password. $level. $email;
		
		$to = $email;
		$subject = "New user information";
		$message = <<< EOT
Welcome {$fname} {$lname} to our CMS. 
Here is your login information:

Username: {$username};
Password: {$password};

Go to <a href="https://phpassign2-taylor132465.c9users.io/admin/admin_login.php">This Link</a> to login.
EOT;

		$headers = 'from: thing@email.com'."\r\n";

		mail($to, $subject, $message, $headers);
		//echo $message;
	}
	
//	newUserEmail('taylor', 'strutton', 'tstrutton2', 'tsrutton', 1, 'taylorstrutton15@gmail.com');
	
?>