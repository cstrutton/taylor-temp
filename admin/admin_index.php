<?php
	//ini_set('display_errors', 1);
	//error_reporting(E_ALL);
	require_once('phpscripts/init.php');
	//confirm_logged_in();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title> Welcome to your Admin Panel</title>
<link rel="stylesheet" href="../../css/foundation.css" type="text/css" />
<link rel="stylesheet" href="css/admin_index.css" type="text/css" />
<script type="text/javascript" src="../../js/vendor/foundation.js"></script>
<script type="text/javascript" src="../../js/vendor/jquery.js"></script>
<script type="text/javascript" src="../../js/vendor/what-input.js"></script>
</head>
<body>
	
	<header class="expanded row">
		<h1>Welcome <?php echo $_SESSION['user_fname'];  ?> to your admin panel</h1>
	</header>
	
	<div class="expanded row">
		<div class="large-2 columns" id="menu">
			    <a href="admin_createuser.php">Create New User</a>
			    <a href="admin_edituser.php">Edit Account</a>
			    <a href="#">Change Password</a>
			    <a href="phpscripts/caller.php?caller_id=logout">Sign Out</a>
	    </div>
    </div>
    
    
    <script type="text/javascript" src="../../js/app.js"></script>
</body>
</html>