<?php
	require_once("phpscripts/init.php");
	//confirm_logged_in();
	
	//ini_set('display_errors',1);
	//error_reporting(E_ALL);
	
	$id = $_SESSION['users_creds'];
	$popForm = getUser($id);
	
	if(isset($_POST['submit'])) {
		//echo "works";
		$fname = trim($_POST['fname']);
		$lname = trim($_POST['lname']);
		$username = trim($_POST['username']);
		$password = trim($_POST['password']);
		$email = trim($_POST['email']);
		
		$result = editUser($id, $fname, $lname, $username, $password, $email);
		$message = $result;
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Account</title>
<link rel="stylesheet" href="../../css/foundation.css" type="text/css" />
<link rel="stylesheet" href="css/admin_index.css" type="text/css" />
<script type="text/javascript" src="../../js/vendor/foundation.js"></script>
<script type="text/javascript" src="../../js/vendor/jquery.js"></script>
<script type="text/javascript" src="../../js/vendor/what-input.js"></script>
</head>
<body>
	
	<header class="expanded row">
		<h1>Welcome <?php echo $_SESSION['user_fname'];  ?> to your admin panel</h1>
	</header>
	
	<div class="expanded row">
		<div class="large-2 columns" id="menu">
			    <a href="admin_createuser.php">Create New User</a>
			    <a href="admin_edituser.php">Edit Account</a>
			    <a href="#">Change Password</a>
			    <a href="phpscripts/caller.php?caller_id=logout">Sign Out</a>
	    </div>
    <div class="large-3 large-offset-3 columns end" id="edit">
	<h3>Edit Account</h3>
    <?php if(!empty($message)){echo $message;} ?>
	<form action="admin_edituser.php" method="post">
		<label>First Name:</label>
        <input type="text" name="fname" value="<?php echo $popForm['user_fname']; ?>">
        <label>Last Name:</label>
        <input type="text" name="lname" value="<?php echo $popForm['user_lname']; ?>">
        <label>Username:</label>
        <input type="text" name="username" value="<?php echo $popForm['user_name']; ?>">
        <label>Email:</label>
        <input type="email" name="email" value="<?php echo $popForm['user_email']; ?>">
        <input type="submit" name="submit" value="Edit Account" class="button">
	</form>
	</div>
	</div>
</body>
</html>